class: impact

# PostgreSQL
# in real life

---

## Pau Pérez

@prez_pau

Tech (mainly) at Coopdevs

Working on:
* Open Food Network
* TimeOverflow
* Donalo.org
* Som Connexió
* ...

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/coopdevs.jpg)

## Coopdevs

.big[*Software lliure per a l'economia social*]

---

class: impact

## What is PostgreSQL?

---

### What is PostgreSQL?

A free and open source object-relation DBMS

---

### What is PostgreSQL?

Relational<br>
Extensible<br>
SQL-standard compliant (mostly)<br>
ACID through MVCC<br>
Transactions<br>
Procedural language<br>
Triggers<br>
Views<br>
Replication

---

### What is PostgreSQL?

Concurrent DDL<br>
Rules<br>
CTEs<br>
Full-text search<br>
Pub/Sub<br>
TOAST (The Oversized-Attribute Storage Technique)<br>
Foreign Data Wrappers<br>
Parallel query execution<br>
JIT

---

### What is PostgreSQL?

Ingres, University of California, Berkeley, 1982<br>
POSTGRES project starts in Berkeley, 1985<br>
POSTGRES v1, 1989<br>
MIT license, 1994<br>
Replaced POSTQUEL with SQL + psql, 1995<br>
Renamed to PostgreSQL and life outside Berkeley, 1996<br>
v6.0 and PostgreSQL Global Development Group, 1997

---

### Open Source

No vendor lock-in + Rich ecosystem + Development pace

---

class: middle
## Partial indexes

---

### Theory

```
CREATE UNIQUE INDEX ON users (email) WHERE deleted_at is null;
```

---

### Real life

Som Connexió needs to match the consumptions to their invoice lines

--

Mas Móvil lets them export the monthly consumptions

--

`account_invoice_line` has a polymorphic column

--

We need to find the appropriate invoice lines that match the Mas Móvil data

---

### Real life

```
CREATE INDEX CONCURRENTLY account_invoice_line_product_partial_idx
   ON account_invoice_line (product)
   WHERE origin LIKE 'contract.consumption,%';"
```

---

### Real life

Mediation process X times faster => Happy customer

---

class: middle

## Full-text search

---

### Theory

`tsvector`: represents a document in a form optimized for text search

`tsquery`: represents a text query

---

### Theory

```
SELECT 'a fat cat sat on a mat rat'::tsvector @@ 'cat & rat'::tsquery;
```

---

### Theory

Full text search performs:

* Parsing documents into tokens
* Converting tokens into lexemes
* Storing preprocessed documents optimized for searching

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/text_search.png)

### Real life

---

### Real life

TimeOverflow's operational costs &darr;&darr; => We can contribute to the project

---

class: middle

## pg\_stat_statements

---

### Theory

Separate module that tracks SQL statements' execution

It comes with its view and utility functions

---

### Theory

```
SELECT query, calls, total_time, rows, 100.0 * shared_blks_hit /
       nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent
  FROM pg_stat_statements
  ORDER BY total_time DESC LIMIT 5;

query       | INSERT INTO "sessions" ("created_at", "data", "session_id", ...
calls       | 5
total_time  | 83.215
rows        | 5
hit_percent | 73.5294117647058824
```

---

### Real life

Our users are annoyed with the slowness of the app.<br>
**Where should we focus our efforts?**

---

### Real life

```
query       | SELECT COUNT(*) FROM "order_cycles"
              LEFT OUTER JOIN exchanges
                ON (exchanges.order_cycle_id = order_cycles.id)
              LEFT OUTER JOIN enterprises
                ON (enterprises.id = exchanges.sender_id
                  OR enterprises.id = exchanges.receiver_id)
              WHERE order_cycles.coordinator_id IN (?)
               OR enterprises.id IN (?)
calls       | 30040
total_time  | 626381.9099999
rows        | 30040
hit_percent | 100.0000000000000000
```

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/pg_stat_statements.png)

### Real life

---

### Real life

We spend time on where we can improve the most => The app will be noticeable faster

---

class: middle

## pg\_stat_activity

---

### Theory

```
datid            | 16386
datname          | openfoodnetwork
pid              | 2763
usesysid         | 16384
usename          | ofn_user
application_name | delayed_job
client_addr      | ::1
client_hostname  | 
client_port      | 57372
backend_start    | 2019-06-05 07:52:16.372732+00
xact_start       | 
query_start      | 2019-06-05 09:40:43.282826+00
state_change     | 2019-06-05 09:40:43.283123+00
waiting          | f
state            | idle
backend_xid      | 
backend_xmin     | 
query            | UPDATE "delayed_jobs" SET locked_at = ...
```

---

### Real life

What the hell is going on!

```
# select query from pg_stat_activity where pid = 1824;
-[ RECORD 1 ]----+-------------------------------------------------------------------------------------------------------------------------------------
query            | SELECT "a"."id" AS "id"
                   FROM "import_csv_log" AS "a"
                   WHERE (("a"."parent" IN (3642820)))
                   ORDER BY "a"."date_time" DESC, "a"."id" ASC`
```

<br>
<br>
https://github.com/coopdevs/handbook/wiki/Investigar-queries-lentas

---

### Real life

Successful disaster management => Not-so-annoyed users

---

class: middle

## CHECK constraints

---

### Theory

```
CREATE TABLE products (
   (...)
   sale_price numeric CHECK (sale_price > 0),
   CHECK (price > sale_price)
);
```

---

### Theory

```
CREATE TABLE fibonacci (i int CHECK (is_fib(i)));
```
being is_fib a user-defined function

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/check.png)

### Real life

https://github.com/coopdevs/timeoverflow/pull/421

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/check2.png)

### Real life

https://github.com/coopdevs/timeoverflow/pull/348

---

class: middle

## PostgreSQL monitoring

---

### Theory

Statistics collector + powerful visualizations = observability

---

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/dashboard.png)

### Real life

---

class: middle

## Q&A

---

## Session resources

[gitlab.com/coopdevs/postgresql-in-real-life](https://gitlab.com/coopdevs/postgresql-in-real-life)<br>

[coopdevs.org](http://coopdevs.org)<br>
[github.com/coopdevs](https://github.com/coopdevs)<br>
[gitlab.com/coopdevs](https://gitlab.com/coopdevs)<br>
[community.coopdevs.org](https://community.coopdevs.org)

---
