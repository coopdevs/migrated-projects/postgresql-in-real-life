# PostgreSQL in real life

Slides of the lecture for the Computer Science degree at TecnoCampus Mataró.

This lecture is heavily based on https://gitlab.com/coopdevs/postgresql-workshop-talk.

PDF file: https://speakerdeck.com/coopdevs/postgresql-in-real-life
